import { createStackNavigator , createBottomTabNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import React from 'react';
import Category from './screens/Category';
import Categories   from './screens/Categories';
import Login from './screens/Login';
import Cart from './screens/Cart';
import Setting from './screens/Setting';
import Order from './screens/Order';
const color = {
    ACTIVE : '#147efb',
    INACTIVE: '#ccc',
}
const LoginStack = createStackNavigator({
    Login,
});
const CategoryStack  = createStackNavigator({ 
    Categories,
    Category
});
CategoryStack.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarIcon: ({ focused }) => {
        return <Icon name="ios-planet"
        size={36}
        color={ focused ? color.ACTIVE : color.INACTIVE }
        /> 
    }
};
const CartStack = createStackNavigator({ 
    Cart
});
CartStack.navigationOptions = {
    tabBarLabel: 'Cart',
    tabBarIcon: ({ focused }) => {
        return <Icon name="ios-cart"
        size={36}
        color={ focused ? color.ACTIVE : color.INACTIVE }
        /> 
    }
}
const OrderStack = createStackNavigator({ Order });
OrderStack.navigationOptions = {
    tabBarLabel: 'Order',
    tabBarIcon: ({ focused }) => {
        return <Icon name="ios-cog"
        size={36}
        color={ focused ? color.ACTIVE : color.INACTIVE }
        /> 
    }
}
const SettingStack = createStackNavigator({ Setting });
SettingStack.navigationOptions = {
    tabBarLabel: 'Setting',
    tabBarIcon: ({ focused }) => {
        return <Icon name="ios-wallet"
        size={36}
        color={ focused ? color.ACTIVE : color.INACTIVE }
        /> 
    }
}
const AppNavigator = createBottomTabNavigator({
    LoginStack,
    CategoryStack,
    CartStack,
    OrderStack,
    SettingStack
})
export default AppNavigator;