import React from 'react';
import { View, Text, StyleSheet,Image,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
export default function CartItems(props) {
        const { product } = props;
        console.log(product);
        return (
            <View style={styles.container}>
                <Image style={styles.img}  source={require('../assets/product.jpg')}/>
                <View style={styles.info}>
                    <Text style={styles.name}>{product.name}</Text>
                    <Text style={styles.price}>{product.price}</Text>
                </View>
                
                <View style={styles.icon}> 
                    <TouchableOpacity>
                        <Icon name="ios-add-circle" size={36} color="#3e47588c"/>
                    </TouchableOpacity>
                    <Text style={styles.quality}> {product.quality} </Text>
                    <TouchableOpacity>
                        <Icon name="ios-remove-circle" size={36} color="#3e47588c"/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    
}
function addToCart(product) {

}
function removeFromCart(product) {

}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        padding: 16,
        borderRadius: 4,
        shadowColor: '#000',
        backgroundColor: '#FFF',
        shadowRadius: 10, 
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.3, 
        elevation: 10,
        marginBottom: 10,
        marginRight: 7,
        marginLeft: 7,
       
    },
    img: {     
        width: 100,
        height: 70,
    },
    info: {
        flexDirection: 'column',
        alignContent: 'center',
        marginLeft: 10,
    },
    icon: {
        flexDirection: 'row',
        marginLeft: 70,
        marginTop: 17,
    },
    name: {
        fontFamily: "sans-serif",
        marginTop: 10,
    },
    price: {
        
        marginTop: 10,
    },
    quality: {
        fontSize: 20,
        paddingTop: 2,
        marginHorizontal: 3,
    }

});