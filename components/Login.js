import React from 'react';
import  {
    ScrollView,
    Text,
    TextInput,
    View,
    Button
} from 'react-native'

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        var {isLoggedIn} = this.props;
    }
    componentDidMount() {

    }
    render() {
        return (
            <ScrollView style={{padding: 20}}>
            <Text 
                style={{fontSize: 27}}>
                Login
            </Text>
            <TextInput placeholder='Username' />
            <TextInput placeholder='Password' />
            <View style={{margin:7}} />
            <Button 
                      onPress={this.props.onLoginPress}
                      title="Submit"
                  />
              </ScrollView>
        );
    }
}