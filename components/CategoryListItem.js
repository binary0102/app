import React from 'react'
import  {
    Image,
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native'
import WorldWide from '../assets/worldwide.png';

export default function CategoryListItem(props) {
    const { category , onPress} = props;
    return (
        <TouchableOpacity activeOpacity={0.5} onPress={onPress
        }>
        <View style={styles.container}> 
        <Text style={styles.title}>{category.name} </Text>
        <Image style={styles.categoryImage} source={WorldWide}/>
        </View>
        </TouchableOpacity>
    );
}
const styles =  StyleSheet.create({
    categoryImage: {
        width: 64,
        height: 64,
    },
    title: {
        textTransform: 'uppercase',
        marginBottom: 8,
        fontWeight: '700',
    },
    container: {
        alignItems: 'center',
        padding: 16,
        borderRadius: 4,
        shadowColor: '#000',
        backgroundColor: '#FFF',
        shadowRadius: 10, 
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.3, 
        elevation: 10,
        marginBottom: 10,
        marginRight: 7,
        marginLeft: 7,
    }
});