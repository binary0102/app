import React from 'react';
import {StyleSheet,View,Text,TouchableOpacity,Image} from 'react-native';

export default function ProductListItem(props) {
    const { product, onAddToCartClick } = props; 
   
    return (
        <View style={styles.shadow}>
            <View style={styles.container}>
                <Image style={styles.img} source={{ uri: product.images[0].url }}/> 
                    <View style={styles.info}>
                        <Text style={styles.name}>{product.name}</Text>
                        <View style={styles.priceRow}> 
                            <Text style={styles.price}>{product.price}</Text>
                            <TouchableOpacity onPress={onAddToCartClick}>
                                <Text style={styles.cartText}>MUA +</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    cartText: {
        textTransform: 'uppercase',
        fontSize: 16,
        color: '#2f95dc',
    },
    shadow:  {  
        elevation: 10,
        shadowColor: '#000',
        backgroundColor: '#FFF',
        shadowOpacity: 0.3,
        shadowRadius: 10, 
        shadowOffset: { with: 0, height: 0 },
    },
    container: {
         
        borderRadius: 4,
        borderColor: '#FFF',
        overflow: 'hidden',
    },
    info: {
        padding: 8,
    },  
    img: {
        height: 150, 
        borderTopLeftRadius:4,
        borderBottomRightRadius: 4,
    },
    name: {
        fontSize: 14,
        marginBottom: 8,
    }, 
    priceRow: {
      flexDirection: 'row',
      alignContent: 'center'  
    },
    price: {
        fontSize: 15,
        color: '#888',
        flex: 1,
    },
});