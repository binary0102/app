import React, { Component } from 'react';
import {
    Text,
    FlatList,
} from 'react-native';
import CartItems from '../components/CartItems';
export default class Cart extends Component {
    constructor(props){
        super(props);
        this.state = {
            items: [
                {
                    id: 1, 
                    images: [
                      {
                        url: 'https://res.cloudinary.com/dkxdfcf5d/image/upload/v1547473641/samples/ecommerce/leather-bag-gray.jpg',
                      }
                    ],
                    name: "TEST",
                    price: 500000,
                    quality: 1,
                  },
                  {
                    id: 2, 
                    images: [
                      {
                        url: 'https://res.cloudinary.com/dkxdfcf5d/image/upload/v1547473641/samples/ecommerce/leather-bag-gray.jpg',
                      }
                    ],
                    name: "TEST",
                    price: 500000,
                    quality: 2,
                  },
             ],
        } 
    };
   
    static navigationOptions = {
        title: 'Cart'
    }

    render() {
        const { items } = this.state;
     
        return <FlatList 
                data={items}
                renderItem={( { item }) => (<CartItems product={item}/>)}
                keyExtractor={(item) => `${item.id}`}
                />
    }
}

