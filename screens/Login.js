import React from 'react'
import * as Expo from "expo"
import  {
    View,
    Text,
    Button,   
} from 'react-native'
import Login from '../components/Login';
import Logout from '../components/Logout'
export default class ScreenLogin extends React.Component {
    static navigationOptions = {
        title: "Đăng nhập",
    }
    constructor(props) {
        super(props);
        this.state =  {
            isLoggedIn: false,
        }
       
    }   
    signIn = async () => {
        try {
            const result = await Expo.Google.logInAsync({
              androidClientId: '548975238741-fmcgs4l3fe7oa6sdnd6lec6tbcnhe227.apps.googleusercontent.com',
              scopes: ['profile', 'email'],
            });
    
            if (result.type === 'success') {
                this.setState({isLoggedIn:true});
                console.log(result.accessToken);
              } else {
                console.log("DADAD");
              }
            } catch(e) {
                console.log("error", e)
            }
    } 
    render() { 
       return (
           <View>
                 {this.state.isLoggedIn ?  <LoggedInPage /> :
                 <LoginPage signIn={this.signIn} />}
           </View>
       );    
    }
}


const LoginPage = props => {
    return (
      <View>
        <Text>Sign In With Google</Text>
        <Button title="Sign in with Google" onPress={() => props.signIn()} />
      </View>
    )
  }

  const LoggedInPage = props => {
    return (
      <View >
        <Text >Welcome:</Text>
      </View>
    )
  }