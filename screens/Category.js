import React from 'react';
import { StyleSheet ,FlatList,View} from 'react-native';
import ProductListItem from '../components/ProductListItem';
export default class Category extends React.Component {
  static navigationOptions = ({ navigation }) => {
      return {
          title: navigation.getParam('categoryName')
      };
  };
  constructor(props){
    super(props);
    this.state = { 
        products: [
          {
            id: 1, 
            images: [
              {
                url: 'https://res.cloudinary.com/dkxdfcf5d/image/upload/v1547473641/samples/ecommerce/leather-bag-gray.jpg',
              }
            ],
            name: "TEST",
            price: 500000,
          },
          {
            id: 2, 
            images: [
              {
                url: 'https://res.cloudinary.com/dkxdfcf5d/image/upload/v1547473641/samples/ecommerce/leather-bag-gray.jpg',
              }
            ],
            name: "TEST",
            price: 500000,
          },
        ]
    }
  }
  render() {
     
      return (
        <FlatList 
          data={this.state.products}
          contentContainerStyle={styles.container}
          numColumns={2}
          renderItem={({ item }) => 
            <View style={styles.wapper}> 
            <ProductListItem product={item} /> 
            </View>
          }
          keyExtractor={(item) => `${item.id}`} 
        />
    );
  }
}
const product = {
  id : '1',
  name: 'Test'
}
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingTop:16
  },
  wapper: {
    flex: 1, 
    paddingHorizontal: 8
  }
});
