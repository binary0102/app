import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import CategoryListItem from '../components/CategoryListItem'
export default class Categories extends React.Component {
  static navigationOptions = {
    title: "Home",
  }
  constructor(props) {
    super(props);
    this.state = {
      categories: [
        {id :1, name: 'Dung cu '},
        {id :2, name: 'Quan ao'},
        {id :3, name: 'May tinh'}
      ]
    }

  }
  render() {
    const { navigation } = this.props;
    const { categories } = this.state;
    return (
            <FlatList 
              data={categories}        
              renderItem={({ item }) => (
                <CategoryListItem 
                  category = {item} 
                  onPress={() => navigation.navigate('Category', {
                    categoryName: item.name,
                  })}
                />
              )}
              keyExtractor={(item) => `${item.id}`}
              contentContainerStyle={ styles.container }
              />
   );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 10, paddingRight:10,paddingTop: 16
  },
});
